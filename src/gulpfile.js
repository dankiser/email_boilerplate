var gulp = require('gulp'),
	sass = require('gulp-sass'),
	plumber = require('gulp-plumber'),
	inlineCss = require('gulp-inline-css'),
	autoprefixer = require('gulp-autoprefixer'),
	html2txt = require('gulp-html2txt'),
	replace = require('gulp-replace-task'),
	watch = require('gulp-watch'),
	notify = require('gulp-notify'),
	zip = require('gulp-zip');


/******************/
//Production Tasks
/******************/

//Compile Sass
gulp.task('styles', function() {
	gulp.src('scss/*.scss')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 7', 'ios 8', 'android 4'))
		.pipe(gulp.dest('css/'))
		.pipe(notify({message: 'Styles task complete'}));
});

//Inline the new CSS file into the HTML
gulp.task('inline', function() {
	return gulp.src('*.html')
		.pipe(plumber())
		.pipe(inlineCss({
			applyStyleTags: true,
			applyLinkTags: true,
			removeStyleTags: false,
			removeLinkTags: true,
			preserveMediaQueries: true
		}))
		.pipe(gulp.dest('html/'))
		.pipe(notify({message: 'Inline task complete'}));
});

//Use to map vars with a url. Don't forget to prefix vars with @@ in HTML
gulp.task('href', function() {
	gulp.src('html/index.html')
		.pipe(replace({
			variables: {
				'key': 'value',
			}
		}))
		.pipe(gulp.dest('../' + version));
});
/*
//Converts the HTML eMail into a text version
gulp.task('txt', function(){
  gulp.src('html/*.html')
	.pipe(html2txt(150)) // optional wordwrap value. 
	.pipe(gulp.dest('text'))
	.pipe(notify({message: 'Text version created'}));
});
*/
//Create watch task
gulp.task('watch', function() {
	gulp.watch('scss/*.scss', ['styles'])
	gulp.watch('*.html', ['inline'])
});

//Creates a default task to run sass, inline, and watch tasks with one command
gulp.task('default', function() {
	gulp.start('styles', 'inline', 'watch');
});

/******************/
//Collection Tasks
/******************/
var version = '';
//Copy image folder
gulp.task('copy-images', function() {
	gulp.src('images/**/*')
	.pipe(gulp.dest('../' + version + '/images'))
});

//Change relative image URLs and paste result in collection folder with images
gulp.task('img-path', function() {
	gulp.src('../' + version + '/*.html')
		.pipe(replace({
			usePrefix: false,
			patterns: [{
				match: '../images',
				replacement: 'images'
			}]
		}))
		.pipe(gulp.dest('../' + version))
});

//Zip everything up
gulp.task('zip', function() {
	gulp.src('../' + version + '/**/*')
		.pipe(zip(version + '.zip'))
		.pipe(gulp.dest('../'))
});

//Putting it all together, now!
gulp.task('collect', function() {
	gulp.start('copy-images', 'img-path', 'zip');
});